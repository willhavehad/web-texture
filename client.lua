DEFAULT_WIDTH = 2048
DEFAULT_HEIGHT = 2048
DEFAULT_TXN = "blank"

DUI = {}

function tellPlayer(msg)
	TriggerEvent('chat:addMessage', { args = { msg } })
end

function createWebTexture(name, url, width, height)
	local txd = CreateRuntimeTxd(name .. 'TXD')
	local txn = name .. 'TXN'
	local duiObj = CreateDui(url, width, height)
	local dui = GetDuiHandle(duiObj)
	DUI[name] = { duiObj, url }
	local tx = CreateRuntimeTextureFromDuiHandle(txd, txn, dui)
end

RegisterCommand("web-texture", function(source, args)
	if (not args[2]) or args[5] then
		tellPlayer("usage: web-texture <name> <url> [<width> [<height>]]")
	else
		local name = args[1]
		local url = args[2]
		local width
		local height
		if (args[3]) then
			width = args[3]
			height = args[4] or width
		else
			width = DEFAULT_WIDTH
			height = DEFAULT_HEIGHT
		end

		createWebTexture(name, url, width, height)
	end
end)

function setUrl(name, url)
	duiEnt = DUI[name]
	dui = duiEnt[1]
	setDuiUrl(dui, url)
end

RegisterCommand("web-url", function(source, args)
	if (not args[2]) or args[3] then
		tellPlayer("usage: web-url <name> <url>")
	else
		local name = args[1]
		local url = args[2]

		setUrl(name, url)
	end
end)

function refreshDui(name)
	duiEnt = DUI[name]
	dui = duiEnt[1]
	duiUrl = duiEnt[2]
	setDuiUrl(dui, duiUrl)
end


RegisterCommand("web-refresh", function(source, args)
	if (not args[1]) or args[2] then
		tellPlayer("usage: web-refresh <name>")
	else
		local name = args[1]

		refreshDui(name)
	end
end)

function replaceTexture(srcTxd, srcTxn, dest)
	AddReplaceTexture(srcTxd, srcTxn, dest .. 'TXD', dest .. 'TXN')
end

RegisterCommand("replace-texture", function(source, args)
	if (not args[2]) or args[4] then
		tellPlayer("usage: replace-texture <name> <TXD> [<TXN>]")
	else
		local name = args[1]
		local txd = args[2]
		local txn = args[3] or DEFAULT_TXN

		replaceTexture(txd, txn, name) 
	end
end)

RegisterCommand("web-replace", function(source, args)
	if (not args[2]) or args[6] then
		tellPlayer("usage: web-replace <url> <TXD> [<TXN> [<width> [<height]]]")
	else
		local url = args[1]
		local txd = args[2]
		local txn = args[3] or DEFAULT_TXN
		local width
		local height
		if (args[4]) then
			width = args[4]
			height = args[5] or width
		else
			width = DEFAULT_WIDTH
			height = DEFAULT_HEIGHT
		end

		createWebTexture(url, url, width, height)
		AddReplaceTexture(txd, txn, url .. 'TXD', url .. 'TXN')
	end
end)
